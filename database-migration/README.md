# Database Migration

## Migration commands

Make sure liquibase property file contains appropriate
user privileges

Run migration

```mvn liquibase:update ```

## TODO
Integrate as part of deployment process. Services should be able to apply outstanding updates
to db.